from random import randint, shuffle
'''Importando shuffle para embaralhar o baralho'''


class Carta:
    '''Classe que define o valor e o naipe da carta.'''
    def __init__(self, naipe, valor):
        self.naipe=naipe
        self.valor=valor
    
    def __str__(self):
        return f'{self.valor} de {self.naipe}'

class Baralho:
    '''Classe que define o baralho do jogo'''
    def __init__(self):
        self.baralho=self.gera_baralho()

    def gera_baralho(self):
        naipes=['paus','ouros','copas','espadas']
        valores=['ás','2','3','4','5','6','7','8','9','10','valete','dama','rei']
        baralho=[]
        for j in naipes:
            for k in valores:
                baralho.append(Carta(j,k))
        return baralho
    
    def mostra_baralho(self):
        for i in self.baralho:
            print(i) 

    def embaralhar(self):
        shuffle(self.baralho)

    def adicionar_carta(self,carta:list):
        '''Recebe uma lista com cartas e adiciona elas ao final do baralho atual'''
        for i in carta:
            self.baralho.append(i)
    
    def joga_carta(self):
        return self.baralho.pop(0)

class Jogador:
    num_jog=0
    def __init__(self, nome):
        self.nome=nome
        self.montante=[]
        self.__class__.num_jog+=1
        self.pontos=0

    def joga_carta(self):
        return self.montante.pop(0)


if __name__ == '__main__':
    valores=['ás','2','3','4','5','6','7','8','9','10','valete','dama','rei']
    baralho=Baralho()
    baralho.embaralhar()
    j1=Jogador(input('Defina o nome do primeiro jogador 1... '))
    j2=Jogador(input('Defina o nome do primeiro jogador 2... '))
    cartas_temp=[]
    for i in range(round(len(baralho.baralho)/Jogador.num_jog)):
        input('Pressione ENTER para prosseguir...\n')

        for j in range(Jogador.num_jog):
            cartas_temp.append(baralho.joga_carta())

        print(f'Cartas puxadas:\n {j1.nome}: {cartas_temp[len(cartas_temp)-2]}\n {j2.nome}: {cartas_temp[len(cartas_temp)-1]}\n')

        if valores.index(cartas_temp[len(cartas_temp)-2].valor)+1 > valores.index(cartas_temp[len(cartas_temp)-1].valor)+1:
            print(f'{j1.nome} venceu essa rodada!\n')
            for carta in cartas_temp:
                j1.montante.append(carta)
                j1.pontos+=(valores.index(carta.valor)+1) 
            cartas_temp=[]
        elif valores.index(cartas_temp[len(cartas_temp)-2].valor)+1 < valores.index(cartas_temp[len(cartas_temp)-1].valor)+1:
            print(f'{j2.nome} venceu essa rodada!\n')
            for carta in cartas_temp:
                j2.montante.append(carta)
                j2.pontos+=(valores.index(carta.valor)+1)
            cartas_temp=[]
        else: print('Ocorreu um empate!\n')

    if cartas_temp != []:
        print('\nOcorreu um empate na jogada final, porém após ',end='')
    else: print('\nApós ',end='')
    if j1.pontos > j2.pontos:
        print(f'a contagem dos pontos {j1.nome} venceu o jogo, com {j1.pontos} pontos.')
    elif j1.pontos < j2.pontos:
        print(f'a contagem dos pontos {j2.nome} venceu o jogo, com {j2.pontos} pontos.')
    else: print('Ocorreu um empate!')
